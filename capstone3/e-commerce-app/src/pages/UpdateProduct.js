import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';
import { Link, useNavigate, useParams } from 'react-router-dom';

export default function UpdateProduct() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [isDisabled, setIsDisabled] = useState(true);

  const navigate = useNavigate();
  const { productId } = useParams();
  // console.log(productId);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((result) => result.json())
      .then((data) => {
        // console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, []);

  useEffect(() => {
    if (name && description && price) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [name, description, price]);

  function updateProduct(event) {
    event.preventDefault();
    // console.log('Form submitted');
    // console.log('Name:', name);
    // console.log('Description:', description);
    // console.log('Price:', price);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
        method: 'PATCH',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: name,
          description: description,
          price: price,
        }),
      })
        .then((result) => result.json())
        .then((data) => {
          // console.log(data);
          if (data) {
            Swal2.fire({
              title: 'Product Updated Successfully',
              icon: 'success',
              text: 'You have successfully updated the product!',
            });
            navigate('/allProducts');
          } else {
            Swal2.fire({
              title: 'Something went wrong',
              icon: 'error',
              text: 'Please try again!',
            });
          }
        })
        .catch((error) => {
          console.log('Error:', error);
          Swal2.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again!',
          });
        });
  }

  return (
    <Container className="mt-5">
      <Row>
        <Col className="col-6 mx-auto">
          <h1 className="text-center textClr">Update product</h1>
          <Form onSubmit={(event) => updateProduct(event)}>
            <Form.Group className="mb-3" controlId="formBasicName">
              <Form.Label className="textClr">Product Name</Form.Label>
              <Form.Control
                type="text"
                value={name}
                onChange={(event) => setName(event.target.value)}
                placeholder="Enter Product Name"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicDescription">
              <Form.Label className="textClr">Description</Form.Label>
              <Form.Control
                type="textarea"
                value={description}
                onChange={(event) => setDescription(event.target.value)}
                placeholder="Enter Product Description"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPrice">
              <Form.Label className="textClr">Price</Form.Label>
              <Form.Control
                type="text"
                value={price}
                onChange={(event) => setPrice(event.target.value)}
                placeholder="Enter Product Price"
              />
            </Form.Group>

            <div className="d-flex justify-content-between">
              <Button variant="primary" type="submit" disabled={isDisabled}>
                Submit
              </Button>
              <Button as={Link} to="/allProducts" variant="primary" className="me-2">
                Back
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
