const jwt = require("jsonwebtoken");

// For JSON web Token
const secret = "ECommerceAPI";

// Creation of Token
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		isAdmin: user.isAdmin,
		email: user.email
	}

	return jwt.sign(data, secret, {});
};

module.exports.verify = (request, response, next) => {

	let token = request.headers.authorization;

	if(token !== undefined){

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if(error){

				return response.send("Auth failed!")

			}else{

				next()
				
			}
		})
	}else{
		return response.send("No Token provided!")
	}
}

// decode the encrypt token
module.exports.decode = (token) => {

	token = token.slice(7, token.length);

	return jwt.decode(token, {complete:true}).payload;
}