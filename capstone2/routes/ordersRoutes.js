// to use express
const express = require("express");

// to access orderControllers
const ordersControllers = require("../controllers/ordersControllers.js");

// to access auth
const auth = require("../auth.js");

// to use router
const router = express.Router();



// routes for retrieving authenticated orders
router.get("/userAllOrders", auth.verify, ordersControllers.getAllOrders);

// route for order checkout
router.post("/checkout", auth.verify, ordersControllers.ordersCheckOut);

// routes for add to cart 
// router.post("/addToCart", ordersControllers.addToCart);





module.exports = router;