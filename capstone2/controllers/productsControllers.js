const Products = require("../models/Products.js");

// const Orders = require("../models/Orders.js");

const bcrypt = require("bcrypt");

const auth = require("../auth.js");


// Controllers

// Controllers for addProduct
module.exports.addProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

		let newProduct = new Products({

			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			isActive: request.body.isActive

		})

		newProduct.save()
		.then(save => response.send(true))
		.catch(error => response.send(false))

	}else{
		return response.send(false)
	}

}

// Controllers for retrieving all products
module.exports.getAllProducts = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

		Products.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false))

	}else{
		return response.send(false)
	}

}

// Controllers for retrieving all active products
module.exports.getActiveProducts = (request, response) => {

	Products.find({isActive:true})
	.then(result => response.send(result))
	.catch(error => response.send(false))

}

// Controllers for retrieving specific product
module.exports.getProduct = (request, response) => {

	const productId = request.params.productId;

	Products.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(false))

}

// Controllers for updating product information
module.exports.updateProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	let updateProduct = {

		name: request.body.name,
		description: request.body.description,
		price: request.body.price

	}

	if(userData.isAdmin){

		Products.findByIdAndUpdate(productId, updateProduct)
		.then(result => response.send(result))
		.catch(error => response.send(false))

	}else{

		return response.send(false)

	}

}

// Controllers for archiving product
module.exports.archiveProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	let archiveProduct = {

		isActive: request.body.isActive

	}

	if(userData.isAdmin){

		Products.findByIdAndUpdate(productId, archiveProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false))

	}else{

		return response.send(false)

	}

}