const Orders = require("../models/Orders.js");

const Products = require("../models/Products.js");

const Users = require("../models/Users.js");

const bcrypt = require("bcrypt")

const auth = require("../auth.js");


// Controllers

// Controllers for retrieving all orders
module.exports.getAllOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

		Orders.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))

	}else{
		return response.send("You don't have access to this route")
	}

}


// Controller for Order Check-out
module.exports.ordersCheckOut = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const productId = request.body.product.productId;

  if (userData.isAdmin) {
    return response.send(false);
  } else {
    Products.findById(productId)
      .then((product) => {
        if (!product) {
          return response.send(false);
        }

        const quantity = 1; // Assuming the quantity is always 1 for checkout
        const totalAmount = product.price * quantity;

        const newOrder = new Orders({
          userId: userData.id,
          product: [
            {
              productId: productId,
              quantity: quantity,
            },
          ],
          totalAmount: totalAmount,
        });

        newOrder
          .save()
          .then((savedOrder) => response.send(true))
          .catch((error) => {
            response.send(false)
          });
      })
      .catch((error) => {
        response.send(false)
      });
  }
};


// Controllers for Add to cart
// module.exports.addToCart = (request, response) => {



// }