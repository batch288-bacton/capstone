// Order Schema
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "User ID is required!"]
	},
	product: [{
		productId: {
			type: String,
			required: [true, "Product ID is required!"]
		},
		quantity: {
			type: Number,
			required: [true, "Quantity is required!"]
		}
	}],
	totalAmount: {
		type: Number,
		required: [true, "Total amount is required!"]
	},
	purchasedOn: {
		type: Date,
		default: new Date
	}

})



// Order model

const Order = mongoose.model("Order", orderSchema);

module.exports = Order;