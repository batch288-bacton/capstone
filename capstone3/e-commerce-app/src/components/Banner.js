import {Button, Container, Row, Col} from 'react-bootstrap';

import {Link} from 'react-router-dom';


export default function Banner(){


	return(
		<Container>
			<Row>
				<Col className = 'mt-3 text-center'>
					<h1 className="textClr">Learn More!</h1>
					<p className="textClr">We offer lots of products depends on your needs!</p>
					<Button as = {Link} to = '/products'>Learn More!</Button>
				</Col>
			</Row>
		</Container>
		)

}