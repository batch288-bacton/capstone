// to use express
const express = require("express");

// to access userControllers
const usersControllers = require("../controllers/usersControllers.js");

// to access auth
const auth = require("../auth.js");

// to use router
const router = express.Router();



// route for register
router.post("/register", usersControllers.registerUser);

// route for login
router.post("/login", usersControllers.loginUser);

// route for retrieving user details
router.get("/details", auth.verify, usersControllers.getUserDetails);

// route for setting user as admin
router.patch("/:userId/setting", auth.verify, usersControllers.setAsAdmin);

// route for retrieving authenticated users order
router.get("/:userId/orders", auth.verify, usersControllers.getAuthUserOrders);











module.exports = router;