import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';
import { Link, useNavigate, Navigate } from 'react-router-dom';

export default function Register() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [mobileNo, setMobileNo] = useState('');

  const { user, setUser } = useContext(UserContext);

  const [isDisabled, setIsDisabled] = useState(true);

  const navigate = useNavigate();

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2 &&
      password1.length > 6 &&
      mobileNo !== '' &&
      mobileNo.length >= 11
    ) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [firstName, lastName, email, password1, password2, mobileNo]);

  function register(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password1,
        mobileNo: mobileNo,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        if (data) {
          console.log(data);
          Swal2.fire({
            title: 'Successfully registered',
            icon: 'success',
            text: 'You have succesfully registered!',
          });
          navigate('/login');
        } else {
          Swal2.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again!',
          });
        }
      });
  }

  useEffect(() => {
    const storedUser = JSON.parse(localStorage.getItem('user'));

    if (storedUser && storedUser.isAdmin) {
      setUser(storedUser);
    } else {
      setUser(null);
    }
  }, []);

  useEffect(() => {
    if (user && user.isAdmin) {
      localStorage.setItem('user', JSON.stringify(user));
    }
  }, [user]);

  return (
    <Container className="mt-5">
      <Row>
        <Col className="col-6 mx-auto">
          <h1 className="text-center textClr">Register</h1>
          <Form onSubmit={register}>
            <Form.Group className="mb-3" controlId="formBasicFirstName">
              <Form.Label className="textClr">First Name</Form.Label>
              <Form.Control
                type="text"
                value={firstName}
                onChange={(event) => setFirstName(event.target.value)}
                placeholder="Enter Your First Name"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicLastName">
              <Form.Label className="textClr">Last Name</Form.Label>
              <Form.Control
                type="text"
                value={lastName}
                onChange={(event) => setLastName(event.target.value)}
                placeholder="Enter Your Last Name"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label className="textClr">Email address</Form.Label>
              <Form.Control
                type="email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                placeholder="Enter email"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword1">
              <Form.Label className="textClr">Password</Form.Label>
              <Form.Control
                type="password"
                value={password1}
                onChange={(event) => setPassword1(event.target.value)}
                placeholder="Password"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword2">
              <Form.Label className="textClr">Confirm Password</Form.Label>
              <Form.Control
                type="password"
                value={password2}
                onChange={(event) => setPassword2(event.target.value)}
                placeholder="Confirm Password"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicMobileNo">
              <Form.Label className="textClr">Mobile Number</Form.Label>
              <Form.Control
                type="text"
                value={mobileNo}
                onChange={(event) => setMobileNo(event.target.value)}
                placeholder="Enter Mobile Number"
              />
            </Form.Group>

            <p className="textClr">
              Have an account already?
              <Link to="/login" style={{ textDecoration: 'none' }}>
                {' '}
                Login here
              </Link>{' '}
            </p>

            <Button variant="primary" type="submit" disabled={isDisabled}>
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
