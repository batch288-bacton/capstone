const Users = require("../models/Users.js");

const bcrypt = require("bcrypt");

const auth = require("../auth.js");

const Orders = require("../models/Orders.js");

// Controllers

// Controllers for register
module.exports.registerUser = (request, response) => {

	Users.findOne({email: request.body.email})
	.then(result => {
		if(result){
			return response.send(false);
		}else{
			let newUser = new Users({

				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin,
				mobileNo: request.body.mobileNo 

			})

			newUser.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false))
		}
	}).catch(error => response.send(false))
};

// Controllers for login
module.exports.loginUser = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {

		if(!result){

			return response.send(false)

		}else{

			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){

				return response.send({
					auth: auth.createAccessToken(result)
				})

			}else{
				return response.send(false)
			}

		}
	}).catch(error => response.send(false));
}

// Controller for getting user details
module.exports.getUserDetails = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData){

		return Users.findById({_id: userData.id})
		.then(result => {

			return response.send(result)

		}).catch(error => response.send(error))

	}else{
		return response.send("You don't have access to this route!")
	}
}

// Controller for setting user as admin

module.exports.setAsAdmin = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send("You don't have access to this route!")
	}else{

		Users.find({isAdmin: true})
		.then(result => response.send(`User is now an Admin!`))
		.catch(error => response.send(error))

	}

}

// Controller for retrieving all authenticated users order
module.exports.getAuthUserOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData){
		return
	}

}